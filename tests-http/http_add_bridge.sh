#!/bin/bash

CLIENT_CERT="/home/pspirek/sslclient/pavel_curl.pem"

echo "--- POST data"
DATA="@conf-input-add-bridge.json"
URL="https://127.0.0.1:8443/restconf/data/ietf-interfaces:interfaces"
curl --http2 -k --cert-type PEM -E $CLIENT_CERT -X POST -d "$DATA" "$URL"

echo "--- conf-commit"
URL="https://127.0.0.1:8443/restconf/operations/jetconf:conf-commit"
curl --http2 -k --cert-type PEM -E $CLIENT_CERT -X POST "$URL"


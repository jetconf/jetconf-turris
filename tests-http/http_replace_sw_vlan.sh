#!/bin/bash

CLIENT_CERT="/home/pspirek/sslclient/pavel_curl.pem"

echo "--- PUT data"
DATA="@conf-input-replace-sw-vlan.json"
URL="https://127.0.0.1:8443/restconf/data/cznic-ethernet-switch:vlans=5/untagged-ports"
curl --http2 -k --cert-type PEM -E $CLIENT_CERT -X PUT -d "$DATA" "$URL"

echo "--- conf-commit"
URL="https://127.0.0.1:8443/restconf/operations/jetconf:conf-commit"
curl --http2 -k --cert-type PEM -E $CLIENT_CERT -X POST "$URL"


from pyroute2 import IPDB

from .swconfig import Swconfig
from .rainbow import RainbowI
from .turrisversion import TurrisVersion

IPDBobj = None  # type: IPDB
SWC = None  # type: Swconfig
RB = None   # type: RainbowI
TV = None   # type: TurrisVersion

from typing import List, Dict, Union, Any
from colorlog import warning as warn, info
from pyroute2 import IPDB

from yangson.instance import InstanceRoute, EntryIndex

from jetconf.data import BaseDatastore, DataChange
from jetconf.helpers import ErrorHelpers, LogHelpers, PathFormat
from jetconf.handler_base import ConfDataObjectHandler, ConfDataListHandler
from jetconf.errors import BackendError

from . import shared_objs as so
from .swconfig import Swconfig
from .turrisversion import TurrisVersion
from .rainbow import Color, LedStatus
from .rainbow_blue import LedIdx as LedIdxBlue
from .rainbow_omnia import LedIdx as LedIdxOmnia

JsonNodeT = Union[Dict[str, Any], List]
epretty = ErrorHelpers.epretty
debug_confh = LogHelpers.create_module_dbg_logger(__name__)


# ---------- User-defined handlers follow ----------

class IfListener(ConfDataListHandler):
    def create_item(self, ii: InstanceRoute, ch: "DataChange"):
        new_ifname = ch.input_data["ietf-interfaces:interface"]["name"]
        new_type = ch.input_data["ietf-interfaces:interface"]["type"]

        if new_type == "cznic-bridge-interface:bridge":
            # Bridge
            so.IPDBobj.create(kind='bridge', ifname=new_ifname)

            bridge_ifs = ch.input_data["ietf-interfaces:interface"].get("cznic-bridge-interface:member-interface", [])
            for bridge_if in bridge_ifs:
                so.IPDBobj.add_port(bridge_if)
        elif new_type == "ietf-interfaces-common:ethSubInterface":
            # VLAN
            new_ifname_splitted = new_ifname.split(".")

            if len(new_ifname_splitted) != 2:
                raise ValueError("VLAN name must be in the form ifX.Y, where ifX is the master interface")

            master_if = new_ifname_splitted[0]
            new_vlan_id = ch.input_data["ietf-interfaces:interface"]["ietf-interfaces-common:encapsulation"]["ietf-if-l3-vlan:dot1q-vlan"]["outer-tag"]["dot1q-tag"]["vlan-id"]
            so.IPDBobj.create(kind='vlan', ifname=new_ifname, link=master_if, vlan_id=new_vlan_id)
        else:
            raise ValueError("Jetconf can only manipulate bridge and vlan interfaces")

        # Add IP addresses
        try:
            ipv4_addrs_base = ch.input_data["ietf-interfaces:interface"]["ietf-ip:ipv4"]["address"]
        except KeyError:
            pass
        else:
            for ipv4_addr in ipv4_addrs_base:
                new_ip = ipv4_addr["ip"]
                prefix = ipv4_addr["prefix-length"]
                so.IPDBobj.interfaces[new_ifname].add_ip(new_ip + "/" + str(prefix))

        # Set operstate
        try:
            opstate = ch.input_data["ietf-interfaces:interface"]["enabled"]
        except KeyError:
            pass
        else:
            so.IPDBobj.interfaces[new_ifname].operstate = "UP" if opstate else "DOWN"

    def replace_item(self, ii: InstanceRoute, ch: "DataChange"):
        base_ii = ii[0:3]
        base_nv_old = self.ds.get_data_root(previous_version=1).goto(base_ii)
        base_nv = self.ds.get_data_root().goto(base_ii[0:-1] + [EntryIndex(base_nv_old.index)])

        current_ifname = base_nv_old["name"].value
        new_ifname = base_nv["name"].raw_value()
        new_type = base_nv["type"].raw_value()

        debug_confh("Replacing interface {} with {}".format(current_ifname, new_ifname))

        # Delete interface
        so.IPDBobj.interfaces[current_ifname].remove()

        if new_type == "cznic-bridge-interface:bridge":
            # Bridge
            so.IPDBobj.create(kind='bridge', ifname=new_ifname)

            try:
                bridge_ifs = base_nv["cznic-bridge-interface:member-interface"].raw_value()
            except KeyError:
                pass
            else:
                for bridge_if in bridge_ifs:
                    so.IPDBobj.add_port(bridge_if)
        elif new_type == "ietf-interfaces-common:ethSubInterface":
            # VLAN
            new_ifname_splitted = new_ifname.split(".")

            if len(new_ifname_splitted) != 2:
                raise ValueError("VLAN name must be in the form ifX.Y, where ifX is the master interface")

            master_if = new_ifname_splitted[0]
            new_vlan_id = base_nv["ietf-interfaces-common:encapsulation"]["ietf-if-l3-vlan:dot1q-vlan"]["outer-tag"]["dot1q-tag"]["vlan-id"].raw_value()
            so.IPDBobj.create(kind='vlan', ifname=new_ifname, link=master_if, vlan_id=new_vlan_id)
        else:
            raise ValueError("Jetconf can only manipulate bridge and vlan interfaces")

        # Add IP addresses
        try:
            ipv4_addrs_base = base_nv["ietf-ip:ipv4"]["address"].raw_value()
        except KeyError:
            pass
        else:
            for ipv4_addr in ipv4_addrs_base:
                new_ip = ipv4_addr["ip"]
                prefix = ipv4_addr["prefix-length"]
                so.IPDBobj.interfaces[new_ifname].add_ip(new_ip + "/" + str(prefix))

        # Set operstate
        try:
            opstate = base_nv["enabled"].raw_value()
        except KeyError:
            pass
        else:
            so.IPDBobj.interfaces[new_ifname].operstate = "UP" if opstate else "DOWN"

    def delete_item(self, ii: InstanceRoute, ch: "DataChange"):
        ifname = ii[2].keys[("name", None)]
        so.IPDBobj.interfaces[ifname].remove()

    def create_list(self, ii: InstanceRoute, ch: "DataChange"):
        pass

    def replace_list(self, ii: InstanceRoute, ch: DataChange):
        pass

    def delete_list(self, ii: InstanceRoute, ch: "DataChange"):
        pass


class IfIpv4Listener(ConfDataListHandler):
    def create_item(self, ii: InstanceRoute, ch: "DataChange"):
        ifname = ii[2].keys[("name", None)]

        new_ip = ch.input_data["ietf-ip:address"]["ip"]
        prefix = ch.input_data["ietf-ip:address"]["prefix-length"]

        so.IPDBobj.interfaces[ifname].add_ip(new_ip + "/" + str(prefix))

    def replace_item(self, ii: InstanceRoute, ch: "DataChange"):
        ifname = ii[2].keys[("name", None)]
        # print((len(base_ii), DataHelpers.ii2str(base_ii)))

        base_ii = ii[0:6]
        base_nv_old = self.ds.get_data_root(previous_version=1).goto(base_ii)
        base_nv = self.ds.get_data_root().goto(base_ii[0:-1] + [EntryIndex(base_nv_old.index)])

        current_ip = base_nv_old["ip"].value
        current_prefix = base_nv_old["prefix-length"].value
        new_ip = base_nv["ip"].value
        new_prefix = base_nv["prefix-length"].value

        debug_confh("Changing IP of {} from {}/{} to {}/{}".format(ifname, current_ip, current_prefix, new_ip, new_prefix))

        so.IPDBobj.interfaces[ifname].del_ip(current_ip + "/" + str(current_prefix))
        so.IPDBobj.interfaces[ifname].add_ip(new_ip + "/" + str(new_prefix))

    def delete_item(self, ii: InstanceRoute, ch: "DataChange"):
        ifname = ii[2].keys[("name", None)]

        base_ii = ii[0:6]
        base_nv_old = self.ds.get_data_root(previous_version=1).goto(base_ii)

        current_ip = base_nv_old["ip"].value
        current_prefix = base_nv_old["prefix-length"].value

        debug_confh("Deleting IP {}/{} of {}".format(current_ip, current_prefix, ifname))

        so.IPDBobj.interfaces[ifname].del_ip(current_ip + "/" + str(current_prefix))

    def create_list(self, ii: InstanceRoute, ch: "DataChange"):
        pass

    def replace_list(self, ii: InstanceRoute, ch: "DataChange"):
        pass

    def delete_list(self, ii: InstanceRoute, ch: "DataChange"):
        pass


class SwConfigGlobalListener(ConfDataObjectHandler):
    def create(self, ii: InstanceRoute, ch: "DataChange"):
        # No need to mess with list keys here, just call replace with faked request ii
        base_ii = self.ds.parse_ii("/cznic-ethernet-switch:switch", PathFormat.URL)
        self.replace(base_ii, ch)

    def replace(self, ii: InstanceRoute, ch: "DataChange"):
        base_ii = ii[0:1]
        base_nv = self.ds.get_data_root().goto(base_ii).value

        swc = Swconfig()
        for key, val in base_nv.items():
            if key == "arl-table":
                continue

            # swconfig key names are the same as in data model
            key_swc = key.replace("-", "_")
            swc.set_global(key_swc, val)

    def delete(self, ii: InstanceRoute, ch: "DataChange"):
        # Keep current config - the switch configuration should never be deleted
        pass


class SwConfigVlanListener(ConfDataListHandler):
    def create_item(self, ii: InstanceRoute, ch: "DataChange"):
        vid = ch.input_data["cznic-ethernet-switch:vlans"]["vid"]

        ports_tagged = ch.input_data["cznic-ethernet-switch:vlans"]["tagged-ports"]
        ports_untagged = ch.input_data["cznic-ethernet-switch:vlans"]["untagged-ports"]

        swc = Swconfig()
        swc.set_vlan_ports(vid, ports_tagged, ports_untagged)

    def replace_item(self, ii: InstanceRoute, ch: "DataChange"):
        vid = int(ii[1].keys[("vid", None)])

        base_ii = ii[0:2]
        base_nv = self.ds.get_data_root().goto(base_ii).value

        ports_tagged = base_nv["tagged-ports"]
        ports_untagged = base_nv["untagged-ports"]

        swc = Swconfig()
        swc.set_vlan_ports(vid, ports_tagged, ports_untagged)

    def delete_item(self, ii: InstanceRoute, ch: "DataChange"):
        vid = int(ii[1].keys[("vid", None)])
        swc = Swconfig()
        swc.set_vlan(vid, "ports", None)


class RainbowListener(ConfDataObjectHandler):
    def create(self, ii: InstanceRoute, ch: "DataChange"):
        # No need to mess with list keys here, just call replace with faked request ii
        base_ii = self.ds.parse_ii("/cznic-rainbow-common:turris-leds", PathFormat.URL)
        self.replace(base_ii, ch)

    def replace(self, ii: InstanceRoute, ch: "DataChange"):
        base_ii = ii[0:1]
        base_nv = self.ds.get_data_root().goto(base_ii).value

        intensity_percent = base_nv.get("intensity")
        if intensity_percent is not None:
            so.RB.set_intensity(intensity_percent)

        for led in base_nv["leds"]:
            led_id = led["name"][0]
            led_color = led.get("color")
            led_status = led.get("status")

            if so.TV == TurrisVersion.BLUE:
                rb_led_idx = {
                    "wan": LedIdxBlue.WAN,
                    "lan": LedIdxBlue.LAN,
                    "wifi": LedIdxBlue.WIFI,
                    "pwr": LedIdxBlue.PWR
                }.get(led_id)
            elif so.TV == TurrisVersion.OMNIA:
                rb_led_idx = {
                    "wan": LedIdxOmnia.WAN,
                    "pwr": LedIdxOmnia.PWR,
                    "lan0": LedIdxOmnia.LAN0,
                    "lan1": LedIdxOmnia.LAN1,
                    "lan2": LedIdxOmnia.LAN2,
                    "lan3": LedIdxOmnia.LAN3,
                    "lan4": LedIdxOmnia.LAN4,
                    "pci1": LedIdxOmnia.PCI1,
                    "pci2": LedIdxOmnia.PCI2,
                    "pci3": LedIdxOmnia.PCI3,
                    "usr1": LedIdxOmnia.USER1,
                    "usr2": LedIdxOmnia.USER2
                }.get(led_id)
            else:
                raise BackendError("Unknown version of Turris router")

            rb_color = {
                "red": Color.RED,
                "blue": Color.BLUE,
                "green": Color.GREEN,
                "white": Color.WHITE,
                "black": (0, 0, 0)
            }.get(led_color)

            if rb_color is None:
                rb_color = (int(led_color[0:2], base=16), int(led_color[2:4], base=16), int(led_color[4:6], base=16))

            so.RB.set_color(rb_led_idx, rb_color)

            rb_status = {
                "on": LedStatus.ON,
                "off": LedStatus.OFF,
                "auto": LedStatus.AUTO
            }.get(led_status)

            so.RB.set_status(rb_led_idx, rb_status)

    def delete(self, ii: InstanceRoute, ch: "DataChange"):
        # Set default color and function
        so.RB.set_intensity(0)
        if so.TV == TurrisVersion.BLUE:
            so.RB.set_color(LedIdxBlue.ALL, Color.DEFAULT10)
            so.RB.set_status(LedIdxBlue.ALL, LedStatus.AUTO)
        elif so.TV == TurrisVersion.OMNIA:
            so.RB.set_color(LedIdxOmnia.ALL, Color.DEFAULTOMNIA)
            so.RB.set_status(LedIdxOmnia.ALL, LedStatus.AUTO)


def commit_begin():
    info("IPDB open")
    so.IPDBobj = IPDB()


def commit_end(failed: bool):
    if failed:
        info("Aborting IPDB transaction")
        try:
            so.IPDBobj.drop()
        except TypeError as e:
            warn(str(e))
    else:
        info("Commiting IPDB transaction")
        so.IPDBobj.commit()

    info("IPDB close")
    so.IPDBobj.release()
    so.IPDBobj = None


RB_HANDLER = None


def register_conf_handlers(ds: BaseDatastore):
    ds.handlers.conf.register(IfListener(ds, "/ietf-interfaces:interfaces/interface"))
    ds.handlers.conf.register(IfIpv4Listener(ds, "/ietf-interfaces:interfaces/interface/ietf-ip:ipv4/address"))
    ds.handlers.conf.register(SwConfigGlobalListener(ds, "/cznic-ethernet-switch:switch"))
    ds.handlers.conf.register(SwConfigVlanListener(ds, "/cznic-ethernet-switch:vlans"))

    global RB_HANDLER
    RB_HANDLER = RainbowListener(ds, "/cznic-rainbow-common:turris-leds")
    ds.handlers.conf.register(RB_HANDLER)

    # Set datastore commit callbacks
    ds.handlers.commit_begin = commit_begin
    ds.handlers.commit_end = commit_end

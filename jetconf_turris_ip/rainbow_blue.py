import os
import mmap

from enum import IntEnum

from .rainbow import RainbowI, LedStatus, LedStateT, Origin, RGBColorT

G_STATUS_REG = 0x23
G_OVERRIDE_REG = 0x22

WAN_COLOR_R = 0x13
WAN_COLOR_G = 0x14
WAN_COLOR_B = 0x15
WAN_MASK = 0x01

LAN_COLOR_R = 0x16
LAN_COLOR_G = 0x17
LAN_COLOR_B = 0x18
LAN_MASK = 0x3E
LAN1_MASK = 0x20
LAN2_MASK = 0x10
LAN3_MASK = 0x08
LAN4_MASK = 0x04
LAN5_MASK = 0x02

WIFI_COLOR_R = 0x19
WIFI_COLOR_G = 0x1A
WIFI_COLOR_B = 0x1B
WIFI_MASK = 0x40
WIFI_HW_STATUS_REG = 0x08

PWR_COLOR_R = 0x1C
PWR_COLOR_G = 0x1D
PWR_COLOR_B = 0x1E
PWR_MASK = 0x80

ALL_MASK = 0xFF

INTENSLVL_REG = 0x20

BASE_REGISTER = 0xFFA00000
MAPPED_SIZE = 4096


class LedRegs:
    def __init__(self, color_r: int, color_g: int, color_b: int, mask: int):
        self.color_r = color_r
        self.color_g = color_g
        self.color_b = color_b
        self.mask = mask

LEDS_REGS = [
    LedRegs(WAN_COLOR_R, WAN_COLOR_G, WAN_COLOR_B, WAN_MASK),
    LedRegs(0, 0, 0, LAN1_MASK),
    LedRegs(0, 0, 0, LAN2_MASK),
    LedRegs(0, 0, 0, LAN3_MASK),
    LedRegs(0, 0, 0, LAN4_MASK),
    LedRegs(0, 0, 0, LAN5_MASK),
    LedRegs(WIFI_COLOR_R, WIFI_COLOR_G, WIFI_COLOR_B, WIFI_MASK),
    LedRegs(PWR_COLOR_R, PWR_COLOR_G, PWR_COLOR_B, PWR_MASK),
    LedRegs(LAN_COLOR_R, LAN_COLOR_G, LAN_COLOR_B, LAN_MASK),
    LedRegs(0, 0, 0, ALL_MASK)
]


class LedIdx(IntEnum):
    WAN = 0
    LAN1 = 1
    LAN2 = 2
    LAN3 = 3
    LAN4 = 4
    LAN5 = 5
    WIFI = 6
    PWR = 7
    LAN = 8
    ALL = 9


class RainbowBlue(RainbowI):
    def __init__(self):
        self._mem_fd = os.open("/dev/mem", os.O_RDWR | os.O_SYNC)
        self._mem = mmap.mmap(self._mem_fd, MAPPED_SIZE, mmap.MAP_SHARED, mmap.PROT_READ | mmap.PROT_WRITE, offset=BASE_REGISTER)

    def __enter__(self):
        return self

    @property
    def override_reg(self) -> int:
        return self._mem[G_OVERRIDE_REG]

    @override_reg.setter
    def override_reg(self, value: int):
        self._mem[G_OVERRIDE_REG] = value

    @property
    def status_reg(self) -> int:
        return self._mem[G_STATUS_REG]

    @status_reg.setter
    def status_reg(self, value: int):
        self._mem[G_STATUS_REG] = value

    def set_intensity(self, percent: int):
        if (percent < 0) or (percent > 100):
            raise ValueError("Intensity must be in range 0-100")

        self._mem[INTENSLVL_REG] = int(7 - ((percent / 100) * 7))

    def set_color(self, led: LedIdx, color: RGBColorT):
        if led not in (LedIdx.WAN, LedIdx.LAN, LedIdx.WIFI, LedIdx.PWR, LedIdx.ALL):
            raise ValueError("Can only set color of WAN, LAN, WIFI, PWR and ALL LEDs")

        if led != LedIdx.ALL:
            self._mem[LEDS_REGS[led].color_r] = color[0]
            self._mem[LEDS_REGS[led].color_g] = color[1]
            self._mem[LEDS_REGS[led].color_b] = color[2]
        else:
            for i in (LedIdx.WAN, LedIdx.LAN, LedIdx.WIFI, LedIdx.PWR):
                self._mem[LEDS_REGS[i].color_r] = color[0]
                self._mem[LEDS_REGS[i].color_g] = color[1]
                self._mem[LEDS_REGS[i].color_b] = color[2]

    def set_status(self, led: LedIdx, status: LedStatus):
        if status == LedStatus.OFF:
            self._mem[G_OVERRIDE_REG] |= LEDS_REGS[led].mask
            self._mem[G_STATUS_REG] |= LEDS_REGS[led].mask
        elif status == LedStatus.ON:
            self._mem[G_OVERRIDE_REG] |= LEDS_REGS[led].mask
            self._mem[G_STATUS_REG] &= ~LEDS_REGS[led].mask
        elif status == LedStatus.AUTO:
            self._mem[G_OVERRIDE_REG] &= ~LEDS_REGS[led].mask

    def set_bargraph(self, count: int, origin: Origin=Origin.LEFT):
        if (count < 0) or (count > 8):
            raise ValueError("LED count must be in range 0-8")

        self._mem[G_OVERRIDE_REG] = ALL_MASK
        gr_mask = 0xFF

        if origin == Origin.LEFT:
            while count:
                gr_mask &= ~LEDS_REGS[count - 1].mask
                count -= 1
        elif origin == Origin.RIGHT:
            while count:
                gr_mask &= ~LEDS_REGS[8 - count].mask
                count -= 1

        self._mem[G_STATUS_REG] = gr_mask

    def save_state(self) -> LedStateT:
        return self.override_reg, self.status_reg

    def restore_state(self, state: LedStateT):
        self.override_reg, self.status_reg = state

    def close(self):
        if self._mem is not None:
            self._mem.close()
            self._mem = None

        if self._mem_fd > 0:
            os.close(self._mem_fd)
            self._mem_fd = -1

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

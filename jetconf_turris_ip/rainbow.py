import time
import threading

from typing import Tuple, Any
from enum import IntEnum

RGBColorT = Tuple[int, int, int]
MorseTimingsT = Tuple[float, float, float, float]
LedStateT = Any


class LedIdx(IntEnum):
    pass


class LedStatus(IntEnum):
    OFF = 0
    ON = 1
    AUTO = 2


class Origin(IntEnum):
    LEFT = 0
    RIGHT = 1


class Color:
    RED = (255, 0, 0)
    GREEN = (0, 255, 0)
    BLUE = (0, 0, 255)
    WHITE = (255, 255, 255)
    DEFAULT10 = (51, 255, 51)
    DEFAULT11 = (255, 255, 255)
    DEFAULTOMNIA = (255, 255, 255)

    @staticmethod
    def from_hsv(h: float, s: float, v: float) -> RGBColorT:
        if (s < 0) or (s > 1) or (v < 0) or (v > 1):
            raise ValueError("Saturation and value has to be in range 0-1")

        h = divmod(h, 360)[1]
        c = v * s
        h_ = h / 60
        x = c * (1 - abs(divmod(h_, 2)[1] - 1))

        if (h_ >= 0) and (h_ < 1):
            (r1, g1, b1) = (c, x, 0)
        elif (h_ >= 1) and (h_ < 2):
            (r1, g1, b1) = (x, c, 0)
        elif (h_ >= 2) and (h_ < 3):
            (r1, g1, b1) = (0, c, x)
        elif (h_ >= 3) and (h_ < 4):
            (r1, g1, b1) = (0, x, c)
        elif (h_ >= 4) and (h_ < 5):
            (r1, g1, b1) = (x, 0, c)
        elif (h_ >= 5) and (h_ < 6):
            (r1, g1, b1) = (c, 0, x)
        else:
            (r1, g1, b1) = (0, 0, 0)

        m = v - c
        (r, g, b) = map(lambda _x: int(_x * 255), (r1 + m, g1 + m, b1 + m))

        return r, g, b

    @staticmethod
    def turris10_weight(color: RGBColorT) -> RGBColorT:
        return int(color[0] * 0.2), color[1], int(color[2] * 0.2)


class RainbowI:
    def __enter__(self):
        return self

    def set_intensity(self, percent: int):
        pass

    def set_color(self, led: LedIdx, color: RGBColorT):
        pass

    def set_status(self, led: LedIdx, status: LedStatus):
        pass

    def set_bargraph(self, count: int, origin: Origin=Origin.LEFT):
        pass

    def save_state(self) -> LedStateT:
        pass

    def restore_state(self, state: LedStateT):
        pass

    def close(self):
        pass

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass


class MorseBlinker(threading.Thread):
    def __init__(self, rainbow: RainbowI, led: LedIdx, text: str, timings: MorseTimingsT = (0.1, 0.3, 0.3, 0.5)):
        super().__init__(target=self._blink, args=(led, text, timings))
        self._rb = rainbow
        self._stop_request = False

    def _blink(self, led: LedIdx, text: str, timings: MorseTimingsT):
        morse_lut = [
            (0, 1),         # a
            (1, 0, 0, 0),   # b
            (1, 0, 1, 0),   # c
            (1, 0, 0),      # d
            (0, ),          # e
            (0, 0, 1, 0),   # f
            (1, 1, 0),      # g
            (0, 0, 0, 0),   # h
            (0, 0),         # i
            (0, 1, 1, 1),   # j
            (1, 0, 1),      # k
            (0, 1, 0, 0),   # l
            (1, 1),         # m
            (1, 0),         # n
            (1, 1, 1),      # o
            (0, 1, 1, 0),   # p
            (1, 1, 0, 1),   # q
            (0, 1, 0),      # r
            (0, 0, 0),      # s
            (1, ),          # t
            (0, 0, 1),      # u
            (0, 0, 0, 1),   # v
            (0, 1, 1),      # w
            (1, 0, 0, 1),   # x
            (1, 0, 1, 1),   # y
            (1, 1, 0, 0)    # z
        ]

        # Save LEDs state
        state_bak = self._rb.save_state()

        self._rb.set_status(led, LedStatus.OFF)
        time.sleep(1)

        for c in text.lower():
            if self._stop_request:
                break

            if c == ' ':
                pass
            elif (ord(c) >= ord('a')) and (ord(c) <= ord('z')):
                for m in morse_lut[ord(c) - ord('a')]:
                    if m:
                        # Long
                        self._rb.set_status(led, LedStatus.ON)
                        time.sleep(timings[1])
                        self._rb.set_status(led, LedStatus.OFF)
                    else:
                        # Short
                        self._rb.set_status(led, LedStatus.ON)
                        time.sleep(timings[0])
                        self._rb.set_status(led, LedStatus.OFF)

                    # Inter-pulse delay
                    time.sleep(timings[2])
            else:
                continue

            # Inter-character delay
            time.sleep(timings[3])

        # Restore LEDs state
        self._rb.restore_state(state_bak)

    def stop(self):
        self._stop_request = True

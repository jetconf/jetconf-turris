from colorlog import info

from jetconf.helpers import PathFormat
from jetconf.errors import BackendError

from . import shared_objs as so, usr_conf_data_handlers
from .swconfig import Swconfig
from .turrisversion import TurrisVersion
from .rainbow_blue import RainbowBlue
from .rainbow_omnia import RainbowOmnia


def jc_startup():
    info("Backend: init")

    # Create global API objects
    so.SWC = Swconfig()

    so.TV = TurrisVersion.get()
    if so.TV == TurrisVersion.BLUE:
        so.RB = RainbowBlue()
    elif so.TV == TurrisVersion.OMNIA:
        so.RB = RainbowOmnia()
    else:
        raise BackendError("This backend can only run on a Turris router!")

    # Apply LED settings at startup
    # Just call replace method of its handler with faked request ii
    rbh = usr_conf_data_handlers.RB_HANDLER
    rbh_fake_ii = rbh.ds.parse_ii("/cznic-rainbow-common:turris-leds", PathFormat.URL)
    rbh.replace(rbh_fake_ii, None)


def jc_end():
    info("Backend: cleaning up")
    so.RB.close()

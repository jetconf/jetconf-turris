import json

from colorlog import info, warning as warn
from pyroute2 import IPDB

from jetconf.helpers import JsonNodeT

from . import shared_objs as so


def load_network_conf() -> JsonNodeT:
    out_iflist = []

    out_data = {
        "interface": out_iflist
    }

    with IPDB() as ipdb:
        for k, iface in ipdb.interfaces.items():
            if not isinstance(k, int):
                continue

            out_if = {
                "name": iface["ifname"],
                "enabled": (iface["operstate"] == "UP") or ((iface["ifname"] == "lo") and (iface["operstate"] == "UNKNOWN"))
            }

            if iface["kind"] == "bridge":
                out_if["type"] = "cznic-bridge-interface:bridge"
                out_br_ifs = []
                for br_ifindex in iface["ports"]:
                    ipdb_br_ifname = ipdb.interfaces[br_ifindex]["ifname"]
                    # br_if_yangref = "/ietf-interfaces:interfaces/interface[name='{}']".format(ipdb_br_ifname)
                    out_br_ifs.append(ipdb_br_ifname)
                out_if["cznic-bridge-interface:member-interface"] = out_br_ifs
            elif iface["kind"] == "vlan":
                out_if["type"] = "ietf-interfaces-common:ethSubInterface"

                encaps = {
                    "ietf-if-l3-vlan:dot1q-vlan": {
                        "outer-tag": {
                            "dot1q-tag": {
                                "tag-type": "ieee802-dot1q-types:s-vlan",
                                "vlan-id": iface["vlan_id"]
                            }
                        }
                    }
                }

                out_if["ietf-interfaces-common:encapsulation"] = encaps
            else:
                out_if["type"] = "iana-if-type:ethernetCsmacd"

            out_ipv4 = {
                # "mtu": iface["mtu"]
            }

            out_ipv4_addrs = []
            for ip_tuple in iface["ipaddr"]:
                if ":" in ip_tuple[0]:
                    continue

                out_ipv4_addr = {
                    "ip": ip_tuple[0],
                    "prefix-length": ip_tuple[1]
                }

                out_ipv4_addrs.append(out_ipv4_addr)

            out_ipv4["address"] = out_ipv4_addrs
            out_if["ietf-ip:ipv4"] = out_ipv4

            out_ipv6 = {
                # "mtu": iface["mtu"]
            }

            out_ipv6_addrs = []
            for ip_tuple in iface["ipaddr"]:
                if "." in ip_tuple[0]:
                    continue

                out_ipv6_addr = {
                    "ip": ip_tuple[0],
                    "prefix-length": ip_tuple[1]
                }

                out_ipv6_addrs.append(out_ipv6_addr)

            out_ipv6["address"] = out_ipv6_addrs
            out_if["ietf-ip:ipv6"] = out_ipv6

            out_iflist.append(out_if)

    return out_data


if __name__ == "__main__":
    print(json.dumps(load_network_conf(), indent=4))

from colorlog import info

from jetconf.helpers import JsonNodeT
from jetconf.data import BaseDatastore

from . import shared_objs as so
from .rainbow import MorseBlinker

from .turrisversion import TurrisVersion
from .rainbow_blue import LedIdx as LedIdxBlue
from .rainbow_omnia import LedIdx as LedIdxOmnia


# ---------- User-defined handlers follow ----------

class OpHandlersContainer:
    def __init__(self):
        self.morse_thr = None

    def blink_morse_op(self, input_args: JsonNodeT, username: str) -> JsonNodeT:
        text = input_args.get("cznic-rainbow-common:text")
        speed = input_args.get("cznic-rainbow-common:speed")

        timings = (0.1, 0.3, 0.2, 0.5)
        if speed is not None:
            timings = tuple(map(lambda x: x / float(speed), timings))

        info("Called operation 'blink-morse' by user '{}':".format(username))

        if (self.morse_thr is not None) and self.morse_thr.is_alive():
            info("Stopping morse thread")
            self.morse_thr.stop()
            self.morse_thr.join()

        if so.TV == TurrisVersion.BLUE:
            self.morse_thr = MorseBlinker(so.RB, LedIdxBlue.ALL, text, timings)
            self.morse_thr.start()
        elif so.TV == TurrisVersion.OMNIA:
            self.morse_thr = MorseBlinker(so.RB, LedIdxOmnia.ALL, text, timings)
            self.morse_thr.start()


def register_op_handlers(ds: BaseDatastore):
    op_handlers_obj = OpHandlersContainer()
    ds.handlers.op.register(op_handlers_obj.blink_morse_op, "cznic-rainbow-common:blink-morse")

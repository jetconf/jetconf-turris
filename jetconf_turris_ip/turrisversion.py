from enum import Enum


class TurrisVersion(Enum):
    BLUE = 10
    OMNIA = 20
    UNKNOWN = -1

    @classmethod
    def get(cls) -> "TurrisVersion":
        retval = cls.UNKNOWN

        with open("/proc/cpuinfo", "rt") as f:
            eof = False
            while not eof:
                line = f.readline()
                if line == "":
                    eof = True

                if "Marvell Armada 3" in line:
                    retval = cls.OMNIA
                    eof = True
                elif "P2020" in line:
                    retval = cls.BLUE
                    eof = True

        return retval

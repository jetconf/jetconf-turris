import json

from colorlog import error

from yangson.instance import NonexistentInstance

from jetconf.data import JsonDatastore
from jetconf.helpers import ErrorHelpers

from .iface_api import load_network_conf
from .swconfig import Swconfig


class UserDatastore(JsonDatastore):
    def load(self):
        super().load()
        new_root = self._data

        # Read network configuration and save it to the datastore
        try:
            conf_json = load_network_conf()
            new_root = new_root.put_member("ietf-interfaces:interfaces", conf_json, raw=True).top()
        except Exception as e:
            error("Cannot load interfaces configuration, reason: {}".format(ErrorHelpers.epretty(e)))

        # Read switch configuration and save it to the datastore
        try:
            swc = Swconfig()
            switch_conf_raw = swc.show(config_only=True)
            new_root = new_root.put_member("cznic-ethernet-switch:switch", switch_conf_raw["switch"], raw=True).top()
            if switch_conf_raw.get("vlans") is not None:
                new_root = new_root.put_member("cznic-ethernet-switch:vlans", switch_conf_raw["vlans"], raw=True).top()
        except FileNotFoundError:
            error("Cannot load switch configuration, 'swconfig' not present on host system")
        except Exception as e:
            error("Cannot load switch configuration, reason: {}".format(ErrorHelpers.epretty(e)))

        self.set_data_root(new_root)

    def save(self):
        # Only need to save NACM and rainbow data,
        # the rest of configuration is loaded dynamically on startup
        data_raw = {}
        try:
            nacm_raw = self._data["ietf-netconf-acm:nacm"].raw_value()
            data_raw["ietf-netconf-acm:nacm"] = nacm_raw
        except NonexistentInstance:
            # Data branch not present
            pass

        try:
            rainbow_raw = self._data["cznic-rainbow-common:turris-leds"].raw_value()
            data_raw["cznic-rainbow-common:turris-leds"] = rainbow_raw
        except NonexistentInstance:
            # Data branch not present
            pass

        with open(self.json_file, "w") as jfd:
            json.dump(data_raw, jfd, indent=4)
from colorlog import info
from typing import Optional
from netifaces import interfaces, ifaddresses, AF_LINK, AF_INET, AF_INET6

from yangson.instance import InstanceRoute

from jetconf.helpers import JsonNodeT
from jetconf.handler_base import StateDataContainerHandler, StateDataListHandler
from jetconf.data import BaseDatastore

from . import shared_objs as so
from .swconfig import Swconfig
from .turrisversion import TurrisVersion


# ---------- User-defined handlers follow ----------
class IfStateHandler(StateDataContainerHandler):
    def __init__(self, datastore: "BaseDatastore", schema_path: str):
        super().__init__(datastore, schema_path)
        self.member_gen = None  # type: StateDataListHandler

    def generate_node(self, node_ii: InstanceRoute, username: str, staging: bool) -> JsonNodeT:
        iface_member_data = self.member_gen.generate_list(node_ii, username, staging)

        out_data = {
            "interface": iface_member_data
        }

        return out_data


class IfStateListHandler(StateDataListHandler):
    @staticmethod
    def _ipv6_netmask_to_prefix(netmask: str) -> int:
        prefix_bits = 0
        nm = netmask.replace(":", "").lower()
        for i in nm[0:-1]:
            if i != 'f':
                raise ValueError("Invalid value of ipv6 netmask {}".format(netmask))
            prefix_bits += 4

        last_possible = ['0', '8', 'c', 'e', 'f']
        i_last = nm[-1]
        if i_last not in last_possible:
            raise ValueError("Invalid value of ipv6 netmask {}".format(netmask))
        prefix_bits += last_possible.index(i_last)
        return prefix_bits

    @staticmethod
    def _ipv4_netmask_to_prefix(netmask: str) -> int:
        prefix_bits = 0

        try:
            ip_bytes = bytes(map(lambda n: int(n), netmask.split(".")))
            ip_val = int(ip_bytes.hex(), base=16)
            for i in range(32):
                if ip_val & 0x80000000:
                    prefix_bits += 1
                else:
                    break
                ip_val <<= 1
        except ValueError:
            raise ValueError("Invalid value of ipv4 netmask {}".format(netmask))

        return prefix_bits

    def _gen_if_data(self, ifname: str) -> Optional[JsonNodeT]:
        ifinfo_raw = ifaddresses(ifname)

        if_data = {
            "name": ifname,
            "type": "iana-if-type:ethernetCsmacd",
            "oper-status": "up"
        }

        try:
            if_data["phys-address"] = ifinfo_raw[AF_LINK][0]["addr"]
        except KeyError:
            pass

        if len(ifinfo_raw.get(AF_INET, [])) > 0:
            if_ip4_adr_list = []
            for if_ip4 in ifinfo_raw[AF_INET]:
                if_ip4_adr_out = {
                    "ip": if_ip4["addr"],
                    "prefix-length": self._ipv4_netmask_to_prefix(if_ip4["netmask"])
                }
                if_ip4_adr_list.append(if_ip4_adr_out)

            if_ip4_out = {
                # "forwarding",
                # "mtu",
                "address": if_ip4_adr_list
            }

            if_data["ietf-ip:ipv4"] = if_ip4_out

        if len(ifinfo_raw.get(AF_INET6, [])) > 0:
            if_ip6_adr_list = []
            for if_ip6 in ifinfo_raw[AF_INET6]:
                if_ip6_adr_out = {
                    "ip": if_ip6["addr"],
                    "prefix-length": self._ipv6_netmask_to_prefix(if_ip6["netmask"])
                }
                if_ip6_adr_list.append(if_ip6_adr_out)

            if_ip6_out = {
                # "forwarding",
                # "mtu",
                "address": if_ip6_adr_list
            }

            if_data["ietf-ip:ipv6"] = if_ip6_out

        return if_data

    def generate_item(self, node_ii: InstanceRoute, username: str, staging: bool) -> JsonNodeT:
        info("if_state_gen_item, ii = {}".format(node_ii))
        ifname = node_ii[2].keys.get(("name", None))
        if_data = self._gen_if_data(ifname)

        return if_data

    def generate_list(self, node_ii: InstanceRoute, username: str, staging: bool) -> JsonNodeT:
        info("if_state_gen_list, ii = {}".format(node_ii))
        if_data_list_out = []

        for ifname in interfaces():
            if_data = self._gen_if_data(ifname)
            if_data_list_out.append(if_data)

        return if_data_list_out


class SwitchArlListHandler(StateDataListHandler):
    def generate_item(self, node_ii: InstanceRoute, username: str, staging: bool) -> JsonNodeT:
        try:
            port_number = int(node_ii[2].keys.get(("port", None)))
        except ValueError:
            raise ValueError("Port number in ARL table key must be integer")

        swc = Swconfig()
        switch_conf_raw = swc.show()
        arl_data = switch_conf_raw["switch"]["arl-table"]

        arl_records_found = list(filter(lambda x: x["port"] == port_number, arl_data))

        if len(arl_records_found) > 0:
            ret_arl = arl_records_found[0]
        else:
            raise ValueError("Port {} addresses not found in ARL table".format(port_number))

        return ret_arl

    def generate_list(self, node_ii: InstanceRoute, username: str, staging: bool) -> JsonNodeT:
        swc = Swconfig()
        switch_conf_raw = swc.show()
        arl_data = switch_conf_raw["switch"]["arl-table"]
        return arl_data


class SwitchPortsListHandler(StateDataListHandler):
    def generate_item(self, node_ii: InstanceRoute, username: str, staging: bool) -> JsonNodeT:
        try:
            port_number = int(node_ii[1].keys.get(("number", None)))
        except ValueError:
            raise ValueError("Port number in key must be integer")

        swc = Swconfig()
        switch_conf_raw = swc.show_port(port_number)
        port_stats_data = switch_conf_raw["ports"]

        port_records_found = list(filter(lambda x: x["number"] == port_number, port_stats_data))

        if len(port_records_found) > 0:
            ret_port_stats = port_records_found[0]
        else:
            raise ValueError("Port {} stats not found".format(port_number))

        return ret_port_stats

    def generate_list(self, node_ii: InstanceRoute, username: str, staging: bool) -> JsonNodeT:
        swc = Swconfig()
        switch_conf_raw = swc.show()
        ports_data = switch_conf_raw["ports"]

        return ports_data


class HwStateHandler(StateDataContainerHandler):
    def generate_node(self, node_ii: InstanceRoute, username: str, staging: bool) -> JsonNodeT:
        comp_list = []
        out_data = {
            "component": comp_list
        }

        if so.TV == TurrisVersion.BLUE:
            cpuinfo = {
                "name": "CPU",
                "class": "iana-hardware:cpu",
                "mfg-name": "Freescale",
                "model-name": "P2020"
            }
            comp_list.append(cpuinfo)

            mmcinfo = {
                "name": "Flash",
                "class": "iana-hardware:storage-drive",
                "description": "256 MB"
            }
            comp_list.append(mmcinfo)
        elif so.TV == TurrisVersion.OMNIA:
            cpuinfo = {
                "name": "CPU",
                "class": "iana-hardware:cpu",
                "mfg-name": "Marvell",
                "model-name": "Armada 380/385"
            }
            comp_list.append(cpuinfo)

            try:
                with open("/sys/block/mmcblk0/size", "rt") as f:
                    mmcblocks_str = f.read(32)
                    mmc_size_mb = int(mmcblocks_str) * 512 / 1024 / 1024
            except FileNotFoundError:
                pass
            else:
                mmcinfo = {
                    "name": "Flash",
                    "class": "iana-hardware:storage-drive",
                    "description": "{} MB".format(mmc_size_mb)
                }
                comp_list.append(mmcinfo)

        mem_size_mb = 0
        with open("/proc/meminfo", "rt") as f:
            eof = False
            while not eof:
                line = f.readline()
                if line == "":
                    eof = True

                if line.startswith("MemTotal:"):
                    line_cols = line.split()
                    mem_size_mb = int(int(line_cols[1]) / 1024)
                    eof = True

        meminfo = {
            "name": "RAM",
            "class": "iana-hardware:module",
            "description": "{} MB".format(mem_size_mb)
        }
        comp_list.append(meminfo)

        return out_data


# Instantiate state data handlers
def register_state_handlers(ds: BaseDatastore):
    if_sh = IfStateHandler(ds, "/ietf-interfaces:interfaces-state")
    if_slh = IfStateListHandler(ds, "/ietf-interfaces:interfaces-state/interface")
    if_sh.member_gen = if_slh
    sw_porth = SwitchPortsListHandler(ds, "/cznic-ethernet-switch:ports")
    hw_stateh = HwStateHandler(ds, "/ietf-hardware-state:hardware")

    ds.handlers.state.register(if_sh)
    ds.handlers.state.register(if_slh)
    ds.handlers.state.register(sw_porth)
    ds.handlers.state.register(hw_stateh)

    if so.TV == TurrisVersion.BLUE:
        sw_arlh = SwitchArlListHandler(ds, "/cznic-ethernet-switch:switch/arl-table")
        ds.handlers.state.register(sw_arlh)

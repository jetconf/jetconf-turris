import subprocess

from colorlog import warning as warn
from typing import List, Union

from jetconf.helpers import JsonNodeT


SetValueT = Union[str, List[str], List[int], int, bool, None]


class Swconfig:
    def __init__(self, device: str=None, remote_cmd: List[str]=None):
        self.device = device or "switch0"
        self.remote_cmd = remote_cmd or []

    def list(self) -> List[str]:
        sw_devs = []

        ret = subprocess.run(self.remote_cmd + ["swconfig", "list"], stdout=subprocess.PIPE)
        cmd_out_lines = ret.stdout.decode("utf-8").splitlines()
        for cmd_out_line in cmd_out_lines:
            if cmd_out_line.startswith("Found: "):
                sw_name = cmd_out_line[7:].split()[0]
                sw_devs.append(sw_name)

        return sw_devs

    def show(self, config_only: bool=False) -> JsonNodeT:
        ret = subprocess.run(self.remote_cmd + ["swconfig", "dev", self.device, "show"], stdout=subprocess.PIPE)
        cmd_out = ret.stdout.decode("utf-8")
        sw_conf_data = self._parse_show_output(cmd_out)

        # Keep only configuration data
        if config_only:
            try:
                del sw_conf_data["ports"]
            except KeyError:
                pass

            try:
                del sw_conf_data["switch"]["arl-table"]
            except KeyError:
                pass

        return sw_conf_data

    def show_port(self, port: int) -> JsonNodeT:
        ret = subprocess.run(self.remote_cmd + ["swconfig", "dev", self.device, "port", str(port), "show"], stdout=subprocess.PIPE)
        cmd_out = ret.stdout.decode("utf-8")
        sw_conf_data = self._parse_show_output(cmd_out)
        return sw_conf_data

    def show_vlan(self, vlan: int) -> JsonNodeT:
        ret = subprocess.run(self.remote_cmd + ["swconfig", "dev", self.device, "vlan", str(vlan), "show"], stdout=subprocess.PIPE)
        cmd_out = ret.stdout.decode("utf-8")
        sw_conf_data = self._parse_show_output(cmd_out)
        return sw_conf_data

    @staticmethod
    def _parse_show_output(cmd_out: str) -> JsonNodeT:
        cmd_out_lines = cmd_out.splitlines()
        sw_conf_global = {}
        sw_conf = {}
        section_key = None
        attr_key = None
        line_no = 0
        port_obj_out = None
        vlan_obj_out = None

        for l in cmd_out_lines:
            try:
                line_no += 1

                if (len(l) == 0) or l.isspace():
                    continue

                # Section name
                if l.endswith(":") and (l.startswith("Global attributes") or l.startswith("Port ") or l.startswith("VLAN ")):
                    section_key = l[0:-1]

                    if section_key == "Global attributes":
                        sw_conf["switch"] = sw_conf_global
                    elif section_key.startswith("Port "):
                        if sw_conf.get("ports") is None:
                            sw_conf["ports"] = []

                        port_obj_out = {
                            "number": int(section_key[5:]),
                        }
                        sw_conf["ports"].append(port_obj_out)
                    elif section_key.startswith("VLAN "):
                        if sw_conf.get("vlans") is None:
                            sw_conf["vlans"] = []

                        vlan_obj_out = {}
                        sw_conf["vlans"].append(vlan_obj_out)
                    else:
                        sw_conf[section_key] = {}

                # Section key-value
                elif section_key and l.startswith("\t"):
                    attr_key, attr_val = l.split(":", maxsplit=1)
                    attr_key = attr_key.strip()
                    attr_key_out = attr_key.replace("_", "-")
                    attr_val = attr_val.strip()

                    if attr_val == "???":
                        continue

                    if section_key == "Global attributes":
                        if attr_key == "arl_table":
                            sw_conf_global[attr_key_out] = []
                        elif attr_key in ("enable_mirror_rx", "enable_mirror_tx", "enable_vlan", "igmp_snooping", "igmp_v3"):
                            if attr_val == "1":
                                sw_conf_global[attr_key_out] = True
                            elif attr_val == "0":
                                sw_conf_global[attr_key_out] = False
                        else:
                            sw_conf_global[attr_key_out] = int(attr_val)
                    elif section_key.startswith("Port "):
                        if attr_key == "link":
                            link_flags = attr_val.split()

                            for link_flag in link_flags:
                                if link_flag.startswith("link:"):
                                    port_obj_out["link"] = link_flag[5:]
                                elif link_flag.startswith("speed:"):
                                    speed_str = link_flag[6:]
                                    speed = None
                                    for i in range(len(speed_str)):
                                        if not speed_str[i].isdecimal():
                                            speed = speed_str[0:i]
                                            break

                                    port_obj_out["speed"] = speed
                                elif link_flag in ("txflow", "rxflow"):
                                    if port_obj_out.get("flow-control") is None:
                                        port_obj_out["flow-control"] = link_flag[0:2]
                                    else:
                                        port_obj_out["flow-control"] = port_obj_out["flow-control"] + " " + link_flag[0:2]

                        elif attr_key == "mib":
                            if attr_val != "No MIB data":
                                port_obj_out[attr_key_out] = {}
                        elif attr_key == "pvid":
                            if attr_val != "0":
                                port_obj_out["primary-vlan-id"] = int(attr_val)
                        elif attr_key in ("enable_eee", "igmp_snooping"):
                            if attr_val == "1":
                                port_obj_out[attr_key_out] = True
                            elif attr_val == "0":
                                port_obj_out[attr_key_out] = False
                        elif attr_key in ("mask", "qmode"):
                            pass
                        else:
                            port_obj_out[attr_key_out] = int(attr_val)
                    elif section_key.startswith("VLAN "):
                        if attr_key == "ports":
                            ports_splitted = attr_val.split()
                            vlan_obj_out["untagged-ports"] = list(map(lambda x: int(x), filter(lambda x: "t" not in x, ports_splitted)))
                            vlan_obj_out["tagged-ports"] = list(map(lambda x: int(x[0:-1]), filter(lambda x: "t" in x, ports_splitted)))
                        elif attr_key == "vid":
                            vlan_obj_out[attr_key_out] = int(attr_val)
                    else:
                        sw_conf[section_key][attr_key_out] = int(attr_val)

                # List value
                else:
                    # ARL table
                    if section_key and section_key == "Global attributes" and attr_key == "arl_table":
                        mac_key, mac_val = l.split(":", maxsplit=1)
                        mac_key = int(mac_key.strip()[5:])
                        mac_val = mac_val.split()[1]

                        arl_entry_current = None
                        for arl_entry in sw_conf_global[attr_key_out]:
                            if arl_entry["port"] == mac_key:
                                arl_entry_current = arl_entry
                                break

                        if arl_entry_current is None:
                            arl_entry_current = {
                                "port": mac_key,
                                "mac-addresses": []
                            }
                            sw_conf_global[attr_key_out].append(arl_entry_current)

                        arl_entry_current["mac-addresses"].append(mac_val)

                    # Port statistics
                    elif section_key and section_key.startswith("Port ") and attr_key == "mib":
                        mib_key, mib_val = l.split(":", maxsplit=1)
                        mib_key = mib_key.strip()
                        mib_val = mib_val.split()[0]
                        port_obj_out[attr_key_out][mib_key] = int(mib_val)
            except ValueError:
                warn("Error parsing line {}: \"{}\"".format(line_no, l))

        return sw_conf

    @staticmethod
    def _val_format(value: SetValueT) -> str:
        if value is None:
            retval = "''"
        elif isinstance(value, str):
            retval = value
        elif isinstance(value, int):
            retval = str(value)
        elif isinstance(value, bool):
            retval = str(int(value))
        elif isinstance(value, list):
            retval = "'" + " ".join(map(lambda x: str(x), value)) + "'"
        else:
            raise ValueError("Unknown format of swconfig value \"{}\"".format(value))

        return retval

    def set_global(self, key: str, value: SetValueT) -> bool:
        ret = subprocess.run(
            self.remote_cmd + ["swconfig", "dev", self.device, "set", key, self._val_format(value)],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE
        )
        cmd_err_out = ret.stderr.decode("utf-8")
        return False if (ret.returncode != 0) or ("failed" in cmd_err_out) else True

    def set_port(self, port: int, key: str, value: SetValueT) -> bool:
        ret = subprocess.run(
            self.remote_cmd + ["swconfig", "dev", self.device, "port", str(port), "set", key, self._val_format(value)],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE
        )
        cmd_err_out = ret.stderr.decode("utf-8")
        return False if (ret.returncode != 0) or ("failed" in cmd_err_out) else True

    def set_vlan(self, vlan: int, key: str, value: SetValueT) -> bool:
        ret = subprocess.run(
            self.remote_cmd + ["swconfig", "dev", self.device, "vlan", str(vlan), "set", key, self._val_format(value)],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE
        )
        cmd_err_out = ret.stderr.decode("utf-8")
        return False if (ret.returncode != 0) or ("failed" in cmd_err_out) else True

    def set_vlan_ports(self, vlan: int, ports_tagged: List[int], ports_untagged: List[int]) -> bool:
        port_list = ports_untagged + list(map(lambda x: str(x) + "t", ports_tagged))
        retval = self.set_vlan(vlan, "ports", port_list)
        return retval

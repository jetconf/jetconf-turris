from typing import List
from enum import IntEnum

from .rainbow import RainbowI, LedStatus, LedStateT, Origin, RGBColorT

SYS_PATH = "/sys/devices/platform/soc/soc:internal-regs/f1011000.i2c/i2c-0/i2c-1/1-002b/"

LEDS_NAMES = [
    "omnia-led:power/",
    "omnia-led:lan0/",
    "omnia-led:lan1/",
    "omnia-led:lan2/",
    "omnia-led:lan3/",
    "omnia-led:lan4/",
    "omnia-led:wan/",
    "omnia-led:pci1/",
    "omnia-led:pci2/",
    "omnia-led:pci3/",
    "omnia-led:user1/",
    "omnia-led:user2/",
    "omnia-led:all/"
]


class LedIdx(IntEnum):
    PWR = 0
    LAN0 = 1
    LAN1 = 2
    LAN2 = 3
    LAN3 = 4
    LAN4 = 5
    WAN = 6
    PCI1 = 7
    PCI2 = 8
    PCI3 = 9
    USER1 = 10
    USER2 = 11
    ALL = 12
    LAN = 13


class LedStrings:
    def __init__(self, auto: str, brightness: str, color: str):
        self.auto = auto
        self.brightness = brightness
        self.color = color


class LedPaths(LedStrings):
    pass


class LedValues(LedStrings):
    pass


class RainbowOmnia(RainbowI):
    def __init__(self):
        self._led_paths = []    # type: List[LedPaths]
        for ln in LEDS_NAMES:
            led_prefix = SYS_PATH + "leds/" + ln
            led_paths = LedPaths(
                led_prefix + "autonomous",
                led_prefix + "brightness",
                led_prefix + "color",
            )
            self._led_paths.append(led_paths)

        self._path_gl_bright = SYS_PATH + "global_brightness"

    @staticmethod
    def _sysfs_read(path: str) -> str:
        with open(path, "rt") as f:
            retval = f.read(16)

        return retval

    @staticmethod
    def _sysfs_write(path: str, value: str):
        with open(path, "wt") as f:
            f.write(value)

    def set_intensity(self, percent: int):
        if (percent < 0) or (percent > 100):
            raise ValueError("Intensity must be in range 0-100")

        self._sysfs_write(self._path_gl_bright, str(percent))

    def set_color(self, led: LedIdx, color: RGBColorT):
        if led != LedIdx.LAN:
            self._sysfs_write(self._led_paths[led].color, "{} {} {}".format(color[0], color[1], color[2]))
        else:
            for i in range(LedIdx.LAN0, LedIdx.LAN4 + 1):
                self._sysfs_write(self._led_paths[i].color, "{} {} {}".format(color[0], color[1], color[2]))

    def set_status(self, led: LedIdx, status: LedStatus):
        if led != LedIdx.LAN:
            if status == LedStatus.OFF:
                self._sysfs_write(self._led_paths[led].auto, "0")
                self._sysfs_write(self._led_paths[led].brightness, "0")
            elif status == LedStatus.ON:
                self._sysfs_write(self._led_paths[led].auto, "0")
                self._sysfs_write(self._led_paths[led].brightness, "255")
            elif status == LedStatus.AUTO:
                self._sysfs_write(self._led_paths[led].auto, "1")
        else:
            for i in range(LedIdx.LAN0, LedIdx.LAN4 + 1):
                if status == LedStatus.OFF:
                    self._sysfs_write(self._led_paths[i].auto, "0")
                    self._sysfs_write(self._led_paths[i].brightness, "0")
                elif status == LedStatus.ON:
                    self._sysfs_write(self._led_paths[i].auto, "0")
                    self._sysfs_write(self._led_paths[i].brightness, "255")
                elif status == LedStatus.AUTO:
                    self._sysfs_write(self._led_paths[i].auto, "1")

    def set_bargraph(self, count: int, origin: Origin=Origin.LEFT):
        if (count < 0) or (count > 12):
            raise ValueError("LED count must be in range 0-12")

        self.set_status(LedIdx.ALL, LedStatus.OFF)

        if origin == Origin.LEFT:
            while count:
                self._sysfs_write(self._led_paths[count - 1].brightness, "255")
                count -= 1
        elif origin == Origin.RIGHT:
            while count:
                self._sysfs_write(self._led_paths[12 - count].brightness, "255")
                count -= 1

    def save_state(self) -> LedStateT:
        state = []

        for led_paths in self._led_paths[0:-1]:
            led_state = LedValues(
                self._sysfs_read(led_paths.auto),
                self._sysfs_read(led_paths.brightness),
                self._sysfs_read(led_paths.color),
            )
            state.append(led_state)

        return state

    def restore_state(self, state: LedStateT):
        for i in range(0, min(len(state), len(self._led_paths) - 1)):
            self._sysfs_write(self._led_paths[i].auto, state[i].auto),
            self._sysfs_write(self._led_paths[i].brightness, state[i].brightness),
            self._sysfs_write(self._led_paths[i].color, state[i].color),

import json
from jetconf_turris_ip.swconfig import Swconfig

if __name__ == "__main__":
    swconfig = Swconfig(remote_cmd=["ssh", "root@195.113.220.98"])
    # conf_data = swconfig.show_vlan(2)
    # conf_data = swconfig.show_port(2)
    conf_data = swconfig.show()
    # print(json.dumps(conf_data, indent=4))
    # retval = swconfig.set_vlan(2, "ports", "'1 2 3'")
    # print(retval)
    # swconfig.set_vlan(3, "ports", [5, "6t"])
    # conf_data = swconfig.show_vlan(3)
    print(json.dumps(conf_data, indent=4))
    pass
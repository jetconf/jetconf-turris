import time

from jetconf_turris_ip.rainbow import LedStatus, Color
from jetconf_turris_ip.rainbow_omnia import RainbowOmnia, LedIdx

if __name__ == "__main__":
    with RainbowOmnia() as rb:
        rb.set_intensity(100)
        rb.set_status(LedIdx.ALL, LedStatus.OFF)
        rb.set_color(LedIdx.ALL, Color.WHITE)

        # Bargraph test
        # rb.set_bargraph(int(sys.argv[1]), Origin.RIGHT)

        # Scanner
        # try:
        #     while True:
        #         for i in range(8):
        #             rb.set_status(LedIdx.ALL, LedStatus.OFF)
        #             rb.set_status(i, LedStatus.ON)
        #             time.sleep(0.03)
        #         for i in range(7, -1, -1):
        #             rb.set_status(LedIdx.ALL, LedStatus.OFF)
        #             rb.set_status(i, LedStatus.ON)
        #             time.sleep(0.03)
        # except KeyboardInterrupt:
        #     exit(0)

        # Snake
        # try:
        #     while True:
        #         for i in range(0, 9):
        #             rb.set_bargraph(i, Origin.LEFT)
        #             time.sleep(0.03)
        #         for i in range(8, -1, -1):
        #             rb.set_bargraph(i, Origin.RIGHT)
        #             time.sleep(0.03)
        # except KeyboardInterrupt:
        #     exit(0)

        # Rainbow
        try:
            h = 0
            rb.set_status(LedIdx.ALL, LedStatus.ON)
            while True:
                col = Color.from_hsv(h, 0.85, 1)
                # rb.set_color(LedIdx.ALL, Color.turris10_weight(col))
                rb.set_color(LedIdx.ALL, col)
                h += 1
                if h > 360:
                    h = 0
                time.sleep(0.03)
        except KeyboardInterrupt:
            exit(0)